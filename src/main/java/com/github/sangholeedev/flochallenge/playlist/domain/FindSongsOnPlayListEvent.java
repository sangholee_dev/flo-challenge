package com.github.sangholeedev.flochallenge.playlist.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class FindSongsOnPlayListEvent {
    private Long id;
    private Long albumId;
    private Long songId;
}
