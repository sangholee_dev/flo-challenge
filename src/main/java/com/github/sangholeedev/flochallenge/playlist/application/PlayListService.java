package com.github.sangholeedev.flochallenge.playlist.application;

import com.github.sangholeedev.flochallenge.playlist.domain.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class PlayListService {
    private final PlayListRepository playListRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public PlayList insertPlayList(PlayList playList) {
        return playListRepository.save(playList);
    }

    public List<PlayList> findAllByUserId(Long userId) {
        return playListRepository.findAllByUserId(userId);
    }

    public List<PlayList> findAllByUserIdFetchJoinSong(Long userId) {
        return playListRepository.findAllByUserIdFetchJoinSong(userId);
    }

    public PlayList getPlayList(Long id) {
        Optional<PlayList> playList = playListRepository.findById(id);
        if (!playList.isPresent()) {
            throw new IllegalArgumentException();
        }
        return playList.get();
    }

    public void deletePlaylist(Long id) {
        playListRepository.deleteById(id);
    }

    public void putSongEvnet(Long id, Long albumId, Long songId) {
        applicationEventPublisher.publishEvent(new FindSongsOnPlayListEvent(id, albumId, songId));
    }

    @EventListener
    public void putSongOnPlayList(final PutSongsOnPlayListEvent event) {
        PlayList playList = getPlayList(event.getId());
        List<Long> songIds = event.getSongIds();

        if (songIds.isEmpty()) {
            throw new IllegalStateException();
        }

        songIds.forEach(songId -> {
            playList.getSongs().add(new PlayedSong(songId));
        });
        playListRepository.flush();
    }

    public PlayList findByIdFetchJoinSong(Long id) {
        return playListRepository.findByIdFetchJoinSong(id);
    }
}
