package com.github.sangholeedev.flochallenge.playlist.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Embeddable
@Getter
public class PlayedSong {
    private Long id;
    @Transient
    private String title;
    @Transient
    private int track;
    @Transient
    private int length;

    public PlayedSong(final Long id) {
        this.id = id;
    }

    public static PlayedSong valueOf(Long id, String title, int track, int length) {
        return new PlayedSong(id, title, track, length);
    }
}
