package com.github.sangholeedev.flochallenge.playlist.domain;

import java.util.List;

public interface PlayListRepositoryCustom {
    PlayList findByIdFetchJoinSong(Long id);

    List<PlayList> findAllByUserIdFetchJoinSong(Long userId);
}
