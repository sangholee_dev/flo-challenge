package com.github.sangholeedev.flochallenge.playlist.domain;

import com.github.sangholeedev.flochallenge.search.domain.QSong;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public class PlayListRepositoryCustomImpl implements PlayListRepositoryCustom {
    private final JPAQueryFactory jpaQueryFactory;

    @Override
    public PlayList findByIdFetchJoinSong(Long id) {
        QPlayList qPlayList = QPlayList.playList;
        QSong qSong = QSong.song;

        List<Tuple> playList = jpaQueryFactory
                .select(qPlayList.id, qPlayList.name, qPlayList.userId,
                        qSong.id, qSong.title, qSong.track, qSong.length
                )
                .from(qPlayList)
                .join(qSong)
                .on(qPlayList.songs.any().id.eq(qSong.id))
                .where(qPlayList.id.eq(id))
                .fetchJoin()
                .fetch();

        Tuple first = playList.get(0);
        return new PlayList(
                first.get(qPlayList.id)
                , first.get(qPlayList.name)
                , first.get(qPlayList.userId)
                , playList.stream().map(tuple -> new PlayedSong(
                tuple.get(qSong.id)
                , tuple.get(qSong.title)
                , tuple.get(qSong.track)
                , tuple.get(qSong.length)
        )).collect(Collectors.toList())
        );
    }

    @Override
    public List<PlayList> findAllByUserIdFetchJoinSong(Long userId) {
        QPlayList qPlayList = QPlayList.playList;
        QSong qSong = QSong.song;

        List<Tuple> playLists = jpaQueryFactory
                .select(qPlayList.id, qPlayList.name, qPlayList.userId,
                        qSong.id, qSong.title, qSong.track, qSong.length
                )
                .from(qPlayList)
                .join(qSong)
                .on(qPlayList.songs.any().id.eq(qSong.id))
                .where(qPlayList.userId.eq(userId))
                .fetchJoin()
                .fetch();

        List<PlayList> searchPlayLists = new ArrayList<>();

        playLists.forEach(tuple -> {
            PlayList playList = new PlayList(
                    tuple.get(qPlayList.id)
                    , tuple.get(qPlayList.name)
                    , tuple.get(qPlayList.userId)
                    , new ArrayList<>());
            if (!searchPlayLists.contains(playList)) {
                searchPlayLists.add(playList);
            }
        });
        searchPlayLists.forEach(playList -> {
            Stream<Tuple> tupleStream = playLists.stream().filter(tuple -> tuple.get(qPlayList.id).equals(playList.getId()));
            playList.getSongs().addAll(tupleStream.map(tuple -> new PlayedSong(
                    tuple.get(qSong.id)
                    , tuple.get(qSong.title)
                    , tuple.get(qSong.track)
                    , tuple.get(qSong.length))
            ).collect(Collectors.toList()));
        });

        return searchPlayLists;
    }
}
