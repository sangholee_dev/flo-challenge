package com.github.sangholeedev.flochallenge.playlist.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlayListRepository extends JpaRepository<PlayList, Long>, PlayListRepositoryCustom {
    List<PlayList> findAllByUserId(Long userId);
}
