package com.github.sangholeedev.flochallenge.playlist.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode(of = "id")
public class PlayList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long userId;

    @ElementCollection
    private List<PlayedSong> songs = new ArrayList<>();

    public PlayList(String name, Long userId) {
        this(null, name, userId, new ArrayList<>());
    }
}
