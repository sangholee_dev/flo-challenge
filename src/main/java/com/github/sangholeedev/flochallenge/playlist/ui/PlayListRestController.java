package com.github.sangholeedev.flochallenge.playlist.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.sangholeedev.flochallenge.common.ErrorsModel;
import com.github.sangholeedev.flochallenge.playlist.application.PlayListService;
import com.github.sangholeedev.flochallenge.playlist.domain.PlayList;
import lombok.AllArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/playlist", produces = MediaTypes.HAL_JSON_VALUE)
public class PlayListRestController {
    private final PlayListService playListService;
    private final ObjectMapper objectMapper;

    @PostMapping("/{userId}")
    public ResponseEntity createPlayList(@PathVariable Long userId, @RequestBody @Valid PlayListDto playListDto, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest()
                    .body(new ErrorsModel(errors));
        }

        PlayList playList = new PlayList(playListDto.getName(), userId);
        PlayList createdPlayList = playListService.insertPlayList(playList);
        WebMvcLinkBuilder selfLinkBuilder = linkTo(PlayListRestController.class).slash(createdPlayList.getId());
        URI createdUri = selfLinkBuilder.toUri();

        PlayListModel playListModel = new PlayListModel(createdPlayList);
        playListModel.add(linkTo(PlayList.class).withRel("query-playList"));
        playListModel.add(selfLinkBuilder.withRel("add-playList"));
        playListModel.add(selfLinkBuilder.withRel("delete-playList"));

        return ResponseEntity.created(createdUri)
                .body(playListModel);
    }

    @GetMapping("/{userId}")
    public ResponseEntity queryPlayList(@PathVariable Long userId) {
        List<PlayList> playLists = playListService.findAllByUserIdFetchJoinSong(userId);

        if (playLists.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        List<PlayListModel> playListModels = playLists.stream().map(playList -> new PlayListModel(playList)).collect(Collectors.toList());

        return ResponseEntity.ok().body(new CollectionModel<>(playListModels
                , linkTo(methodOn(PlayListRestController.class).queryPlayList(userId)).withSelfRel()));
    }

    @GetMapping(value = {"/{userId}/{id}/add/{albumId}", "/{userId}/{id}/add/{albumId}/{songId}"})
    public ResponseEntity addPlayedSong(@PathVariable Long id, @PathVariable Long albumId, @PathVariable(required = false) Long songId) {
        try {
            playListService.putSongEvnet(id, albumId, songId);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        PlayList playList = playListService.findByIdFetchJoinSong(id);
        PlayListModel playListModel = new PlayListModel(playList);
        return ResponseEntity.ok(playListModel);
    }

    @DeleteMapping("/{userId}/{id}")
    public ResponseEntity deletePlayList(@PathVariable Long id) {
        try {
            playListService.getPlayList(id);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }

        playListService.deletePlaylist(id);
        return ResponseEntity.noContent().build();
    }

}
