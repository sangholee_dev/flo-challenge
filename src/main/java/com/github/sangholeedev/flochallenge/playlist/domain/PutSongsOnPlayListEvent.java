package com.github.sangholeedev.flochallenge.playlist.domain;

import lombok.Getter;

import java.util.List;

@Getter
public class PutSongsOnPlayListEvent {
    private Long id;
    private List<Long> songIds;

    public PutSongsOnPlayListEvent(Long id, List<Long> songIds) {
        this.id = id;
        this.songIds = songIds;
    }
}
