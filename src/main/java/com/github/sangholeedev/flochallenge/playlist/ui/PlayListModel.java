package com.github.sangholeedev.flochallenge.playlist.ui;

import com.github.sangholeedev.flochallenge.playlist.domain.PlayList;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.core.Relation;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Relation(collectionRelation = "playLists", itemRelation = "playList")
public class PlayListModel extends EntityModel<PlayList> {
    public PlayListModel(PlayList playList, Link... links) {
        super(playList, links);
        add(linkTo(PlayListRestController.class).slash(playList.getUserId()).slash(playList.getId()).withSelfRel());
    }
}
