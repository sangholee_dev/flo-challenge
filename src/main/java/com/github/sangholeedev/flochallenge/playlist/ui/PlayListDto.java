package com.github.sangholeedev.flochallenge.playlist.ui;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlayListDto {
    @NotEmpty
    private String name;
    private Long userId;

}
