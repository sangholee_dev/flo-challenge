package com.github.sangholeedev.flochallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FloChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(FloChallengeApplication.class, args);
    }
}
