package com.github.sangholeedev.flochallenge.utils;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.Locale;

@Component
public class LocaleValidator {
    public void validate(String localeString, Errors errors) {
        Locale locale = Locale.forLanguageTag(localeString);
    }
}
