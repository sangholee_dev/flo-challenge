package com.github.sangholeedev.flochallenge.search.ui;

import com.github.sangholeedev.flochallenge.search.application.SearchService;
import com.github.sangholeedev.flochallenge.search.domain.Album;
import com.github.sangholeedev.flochallenge.search.domain.LocaleString;
import com.github.sangholeedev.flochallenge.search.domain.Song;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class SearchRestController {
    private final SearchService searchService;

    private final ModelMapper modelMapper;

    @GetMapping(value = "/search")
    public ResponseEntity search(@RequestParam(value = "title") final String title, @RequestParam(value = "locale") final String locale) {
        List<Song> searchSongs = searchService.searchSongApi(title, LocaleString.valueOf(locale));
        List<Album> searchAlbums = searchService.searchAlbumApi(title, LocaleString.valueOf(locale));

        List<AlbumDto> albumDtos = modelMapper.map(searchAlbums, new TypeToken<List<AlbumDto>>() {
        }.getType());

        List<SongDto> songDtos = modelMapper.map(searchSongs, new TypeToken<List<SongDto>>() {
        }.getType());

        AlbumAndSongSearchDto albumAndSongSearchDto = new AlbumAndSongSearchDto(albumDtos, songDtos);
        return ResponseEntity.ok(albumAndSongSearchDto);
    }

    @GetMapping("/albums")
    public ResponseEntity albums(@RequestParam(value = "locale") final String locale, @PageableDefault(size = 10) Pageable pageable, PagedResourcesAssembler<Album> pagedResourcesAssembler) {
        Page<Album> albumPage = searchService.albumsApi(LocaleString.valueOf(locale), pageable);
        PagedModel<AlbumModel> pagedModel = pagedResourcesAssembler.toModel(albumPage, e -> new AlbumModel(e));

        List<AlbumDto> albumDtos = modelMapper.map(albumPage.getContent(), new TypeToken<List<AlbumDto>>() {
        }.getType());

        AlbumPageDto albumPageDto = new AlbumPageDto(HttpStatus.OK.value(), new CustomPages(albumPage, pagedModel), albumDtos);
        return ResponseEntity.ok(albumPageDto);
    }

}
