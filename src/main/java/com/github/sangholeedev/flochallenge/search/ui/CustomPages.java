package com.github.sangholeedev.flochallenge.search.ui;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.sangholeedev.flochallenge.search.domain.Album;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.PagedModel;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomPages {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String first;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String prev;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String last;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String next;

    public CustomPages(Page<Album> albumPage, PagedModel<AlbumModel> pages) {
        if (!albumPage.isFirst()) {
            this.first = pages.getLink("first").get().getHref();
            this.prev = pages.getPreviousLink().get().getHref();
        }

        if (!albumPage.isLast()) {
            this.last = pages.getLink("last").get().getHref();
            this.next = pages.getNextLink().get().getHref();
        }
    }
}
