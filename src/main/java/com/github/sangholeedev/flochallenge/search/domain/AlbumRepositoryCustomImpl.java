package com.github.sangholeedev.flochallenge.search.domain;

import com.querydsl.core.QueryResults;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public class AlbumRepositoryCustomImpl extends QuerydslRepositorySupport implements AlbumRepositoryCustom {
    private final JPAQueryFactory jpaQueryFactory;

    public AlbumRepositoryCustomImpl(JPAQueryFactory jpaQueryFactory) {
        super(Album.class);
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<Album> searchAlbumApi(String title, String... locales) {
        QAlbum qAlbum = QAlbum.album;
        return jpaQueryFactory.selectFrom(qAlbum)
                .where(qAlbum.title.contains(title)
                        .and(qAlbum.locales.any().locale.in(locales))
                )
                .fetch();
    }

    @Override
    public Page<Album> albumsPageApi(Pageable pageable, String... locales) {
        QAlbum qAlbum = QAlbum.album;
        QueryResults<Album> albumSearchResult = jpaQueryFactory.selectFrom(qAlbum)
                .where(qAlbum.locales.any().locale.in(locales))
                .limit(pageable.getPageSize())
                .offset(pageable.getOffset())
                .fetchResults();

        return new PageImpl<>(albumSearchResult.getResults(), pageable, albumSearchResult.getTotal());
    }
}
