package com.github.sangholeedev.flochallenge.search.domain;

import java.util.List;

public interface SongRepositoryCustom {
    List<Song> searchSongApi(String title, String... locales);
}
