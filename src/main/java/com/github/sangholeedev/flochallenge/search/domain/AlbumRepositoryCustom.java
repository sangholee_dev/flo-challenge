package com.github.sangholeedev.flochallenge.search.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AlbumRepositoryCustom {
    List<Album> searchAlbumApi(String title, String... locales);

    Page<Album> albumsPageApi(Pageable pageable, String... locales);
}
