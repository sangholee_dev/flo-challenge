package com.github.sangholeedev.flochallenge.search.ui;

import lombok.Data;

import java.util.List;

@Data
public class AlbumPageDto {
    private final int statusCode;
    private final CustomPages pages;
    private final List<AlbumDto> albums;

    public AlbumPageDto(int statusCode, CustomPages pages, List<AlbumDto> albums) {
        this.statusCode = statusCode;
        this.pages = pages;
        this.albums = albums;
    }

}
