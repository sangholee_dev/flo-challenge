package com.github.sangholeedev.flochallenge.search.application;

import com.github.sangholeedev.flochallenge.playlist.domain.FindSongsOnPlayListEvent;
import com.github.sangholeedev.flochallenge.playlist.domain.PutSongsOnPlayListEvent;
import com.github.sangholeedev.flochallenge.search.domain.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SearchService {
    private final SongRepository songRepository;
    private final AlbumRepository albumRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public List<Album> searchAlbumApi(final String title, final LocaleString locale) {
        return albumRepository.searchAlbumApi(title, LocaleString.ALL.getLocale(), locale.getLocale());
    }

    public List<Song> searchSongApi(final String title, final LocaleString locale) {
        return songRepository.searchSongApi(title, LocaleString.ALL.getLocale(), locale.getLocale());
    }

    public Page<Album> albumsTestApi(Pageable pageable) {
        return albumRepository.findAll(pageable);
    }

    public Page<Album> albumsApi(final LocaleString locale, Pageable pageable) {
        return albumRepository.albumsPageApi(pageable, LocaleString.ALL.getLocale(), locale.getLocale());
    }

    @EventListener
    public void putSong(final FindSongsOnPlayListEvent event) {
        List<Long> songIds = new ArrayList<>();
        if (event.getSongId() == null) {
            songRepository.findAllByAlbum_Id(event.getAlbumId())
                    .forEach(song -> songIds.add(song.getId()));
            ;
        } else {
            songRepository.findById(event.getSongId())
                    .ifPresent(song -> songIds.add(song.getId()));
        }
        applicationEventPublisher.publishEvent(new PutSongsOnPlayListEvent(event.getId(), songIds));
    }

}
