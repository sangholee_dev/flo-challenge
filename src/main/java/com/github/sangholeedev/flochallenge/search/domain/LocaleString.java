package com.github.sangholeedev.flochallenge.search.domain;

import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
public class LocaleString {
    private String locale;

    public static final LocaleString ALL = LocaleString.valueOf("all");

    public LocaleString() {
    }

    public LocaleString(String locale) {
        this.locale = locale;
    }

    public static LocaleString valueOf(String locale) {
        return new LocaleString(locale);
    }
}
