package com.github.sangholeedev.flochallenge.search.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;

    @ElementCollection(fetch = FetchType.EAGER, targetClass = LocaleString.class)
    @CollectionTable
    private Set<LocaleString> locales = new HashSet<>();

    @OneToMany(mappedBy = "album")
    private List<Song> songs;

}
