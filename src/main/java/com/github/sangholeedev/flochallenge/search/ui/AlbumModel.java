package com.github.sangholeedev.flochallenge.search.ui;

import com.github.sangholeedev.flochallenge.search.domain.Album;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;

public class AlbumModel extends EntityModel<Album> {
    public AlbumModel(Album content, Link... links) {
        super(content, links);
    }
}
