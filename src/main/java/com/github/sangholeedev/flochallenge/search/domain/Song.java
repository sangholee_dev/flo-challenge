package com.github.sangholeedev.flochallenge.search.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private int track;
    private int length;
    @ManyToOne(fetch = FetchType.LAZY)
    private Album album;

}
