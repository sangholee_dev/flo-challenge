package com.github.sangholeedev.flochallenge.search.ui;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AlbumAndSongSearchDto {
    List<AlbumDto> albums;
    List<SongDto> songs;

}
