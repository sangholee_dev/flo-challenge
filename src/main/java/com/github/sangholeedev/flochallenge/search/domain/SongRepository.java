package com.github.sangholeedev.flochallenge.search.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongRepository extends JpaRepository<Song, Long>, SongRepositoryCustom {
    List<Song> findAllByAlbum_Id(Long id);
}
