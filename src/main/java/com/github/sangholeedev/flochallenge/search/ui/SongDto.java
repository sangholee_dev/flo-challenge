package com.github.sangholeedev.flochallenge.search.ui;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SongDto {
    private String title;
    private Long id;
    private int track;
    private int length;
}
