package com.github.sangholeedev.flochallenge.search.domain;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SongRepositoryCustomImpl implements SongRepositoryCustom {
    private final JPAQueryFactory jpaQueryFactory;

    @Override
    public List<Song> searchSongApi(String title, String... locales) {
        QSong qSong = QSong.song;
        QAlbum qAlbum = QAlbum.album;

        return jpaQueryFactory.selectFrom(qSong)
                .innerJoin(qSong.album, qAlbum)
                .where(qSong.title.contains(title)
                        .and(qAlbum.locales.any().locale.in(locales)))
                .fetchJoin()
                .fetch();
    }
}
