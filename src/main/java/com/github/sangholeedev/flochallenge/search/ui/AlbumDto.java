package com.github.sangholeedev.flochallenge.search.ui;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDto {
    private String title;
    private Long id;

    private List<SongDto> songs;
}
