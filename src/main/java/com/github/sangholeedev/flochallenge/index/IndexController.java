package com.github.sangholeedev.flochallenge.index;

import com.github.sangholeedev.flochallenge.playlist.ui.PlayListRestController;
import com.github.sangholeedev.flochallenge.search.ui.SearchRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class IndexController {
    @GetMapping(value = {"/", "/api"}, produces = MediaTypes.HAL_JSON_VALUE)
    public RepresentationModel index() {
        RepresentationModel welcome = new RepresentationModel<>();
        welcome.add(linkTo(methodOn(SearchRestController.class).search(null, null)).withRel("search"));
        welcome.add(linkTo(PlayListRestController.class).withRel("playList"));
        return welcome;
    }
}
