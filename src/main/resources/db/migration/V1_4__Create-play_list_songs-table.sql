create table play_list_songs
(
    play_list_id bigint not null,
    id           bigint null,
    constraint
        foreign key (play_list_id) references play_list (id)
);