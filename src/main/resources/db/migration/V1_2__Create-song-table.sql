create table song
(
    id       bigint auto_increment
        primary key,
    length   int          not null,
    title    varchar(255) null,
    track    int          not null,
    album_id bigint       null,
    index (album_id),
    index (title),
    constraint
        foreign key (album_id) references album (id)
);