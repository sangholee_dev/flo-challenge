create table play_list
(
    id      bigint auto_increment
        primary key,
    name    varchar(255) null,
    user_id bigint       null,
    index (user_id)
);