create table album_locales
(
    album_id bigint       not null,
    locale   varchar(255) null,
    index (locale),
    constraint
        foreign key (album_id) references album (id)
);