create table album
(
	id bigint auto_increment
		primary key,
	title varchar(255) null,
	index (title)
);

