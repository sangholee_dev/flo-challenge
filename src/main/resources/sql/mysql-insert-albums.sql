INSERT INTO album(id, title)
VALUES (1, 'Please Please Me (1963.3)');
INSERT INTO album(id, title)
VALUES (2, 'With The Beatles (1963.11)');
INSERT INTO album(id, title)
VALUES (3, 'A Hard Day''s Night (1964.7), Beatles for Sale (1964.12)');
INSERT INTO album(id, title)
VALUES (4, 'Help! (1965.8)');
INSERT INTO album(id, title)
VALUES (5, 'Rubber Soul (1965.12)');
INSERT INTO album(id, title)
VALUES (6, 'Revolver (1966.8)');
INSERT INTO album(id, title)
VALUES (7, 'Sgt. Pepper''s Lonely Hearts Club Band (1967.6) ,Magical Mystery Tour (1967.11)[2]');
INSERT INTO album(id, title)
VALUES (8, 'The Beatles(aka 화이트 앨범) (1968.11)');
INSERT INTO album(id, title)
VALUES (9, 'Yellow Submarine (1969.1)');
INSERT INTO album(id, title)
VALUES (10, 'Abbey Road (1969.9)');
INSERT INTO album(id, title)
VALUES (11, 'Let It Be (1970.5)');
INSERT INTO album(id, title)
VALUES (12, '유재하 <사랑하기 때문에>(1987)');
INSERT INTO album(id, title)
VALUES (13, '들국화 <들국화(1집)>(1986)');
INSERT INTO album(id, title)
VALUES (14, '신중현과 엽전들 <신중현과 엽전들>(1974)');
INSERT INTO album(id, title)
VALUES (15, '김민기 <김민기(1집)>(1971)');
INSERT INTO album(id, title)
VALUES (16, '산울림 <아니벌써(1집)>(1977)');
INSERT INTO album(id, title)
VALUES (17, '어떤날 ＜어떤날 I＞(1986)');
INSERT INTO album(id, title)
VALUES (18, '산울림 <제2집>(1978)');
INSERT INTO album(id, title)
VALUES (19, '한대수 <멀고 먼 길(1집)>(1974)');
INSERT INTO album(id, title)
VALUES (20, '넥스트 〈The Return Of N.Ex.T Part 1: The Being〉(1994)');
INSERT INTO album(id, title)
VALUES (21, '이상은 <공무도하가>(1995)');
INSERT INTO album(id, title)
VALUES (22, '장필순 <나의 외로움이 널 부를 때(5집)>(1997)');
INSERT INTO album(id, title)
VALUES (23, '김현철 〈김현철 Vol.1〉(1989)');
INSERT INTO album(id, title)
VALUES (24, '이문세 <이문세 4>(1987)');
INSERT INTO album(id, title)
VALUES (25, '시인과 촌장 <푸른 돛(2집)>(1986)');
INSERT INTO album(id, title)
VALUES (26, '사랑과 평화 <한동안 뜸 했었지(1집)>(1977)');
INSERT INTO album(id, title)
VALUES (27, '김현식 〈김현식 III〉(1986)');
INSERT INTO album(id, title)
VALUES (28, '한영애 <바라본다(2집)>(1988)');
INSERT INTO album(id, title)
VALUES (29, '델리 스파이스 〈Deli Spice〉(1997)');
INSERT INTO album(id, title)
VALUES (30, '듀스 〈Force Deux〉(1995)');
INSERT INTO album(id, title)
VALUES (31, '어떤날 <어떤날 II>(1989)');