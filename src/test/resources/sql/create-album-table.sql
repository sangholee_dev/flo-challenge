CREATE TABLE album
(
    id    INT NOT NULL PRIMARY KEY,
    title VARCHAR(81)
);