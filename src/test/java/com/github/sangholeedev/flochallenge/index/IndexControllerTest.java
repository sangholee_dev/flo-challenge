package com.github.sangholeedev.flochallenge.index;

import com.github.sangholeedev.flochallenge.AbstractControllerTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class IndexControllerTest extends AbstractControllerTest {
    @Autowired
    private IndexController indexController;

    @Override
    protected Object controller() {
        return indexController;
    }

    @Test
    @DisplayName("인덱스 요청 성공 테스트")
    public void index() throws Exception {
        mockMvc.perform(get("/api")
                .accept(MediaTypes.HAL_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("search").exists())
        ;
    }
}