package com.github.sangholeedev.flochallenge.search.domain;

import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

class LocaleTypeTest {

    @Test
    public void localeTypeTest() {
        assertThat(Locale.getISOCountries()).contains("KR");

        assertThat(Locale.getISOLanguages()).contains("ja");
        assertThat(Locale.forLanguageTag("ko").getLanguage()).isEqualTo("ko");
//        assertThat(Locale.forLanguageTag("ko").getVariant()).isEqualTo(true);
        assertThat(Locale.KOREA.getLanguage()).isEqualTo(true);
    }
}