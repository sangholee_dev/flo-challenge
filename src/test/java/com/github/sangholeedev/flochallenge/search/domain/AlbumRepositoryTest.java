package com.github.sangholeedev.flochallenge.search.domain;

import com.github.sangholeedev.flochallenge.search.ui.AlbumModel;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AlbumRepositoryTest {
    @Autowired
    private AlbumRepository albumRepository;
    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Test
    @DisplayName("데이터가 들어가있는지 테스트")
    public void findAll() {
        assertThat(albumRepository.count()).isGreaterThan(0);
    }


    private static Stream<Arguments> provideSearchAlbumApi() {
        return Stream.of(
                Arguments.of("Yellow", "ja"),
                Arguments.of("이문세", "ko"),
                Arguments.of("noodle", "en")
        );
    }

    @ParameterizedTest
    @MethodSource("provideSearchAlbumApi")
    @DisplayName("앨범데이터를 이름과 지역으로 필터링하는 테스트")
    public void searchAlbumApi(String title, String locale) {
        QAlbum qAlbum = QAlbum.album;
        List<Album> albums = jpaQueryFactory.selectFrom(qAlbum)
                .where(qAlbum.title.contains(title)
                        .and(qAlbum.locales.any().locale.in(locale, "all"))
                )
                .fetch();

        assertThat(albums.stream().map(album -> new AlbumModel(album))).isNotEmpty();

        assertThat(albums)
                .isNotEmpty()
                .extracting(Album::getTitle)
                .contains(title)
        ;
    }

    @Test
    @DisplayName("앨범 목록을 지역별로 페이징 하는 테스트")
    public void albumsPageApi(String locale) {
        QAlbum qAlbum = QAlbum.album;
    }

}