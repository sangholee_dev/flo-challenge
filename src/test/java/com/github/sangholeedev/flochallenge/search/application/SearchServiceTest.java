package com.github.sangholeedev.flochallenge.search.application;

import com.github.sangholeedev.flochallenge.search.domain.Album;
import com.github.sangholeedev.flochallenge.search.domain.LocaleString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SearchServiceTest {
    @Autowired
    private SearchService searchService;

    @Test
    void searchAlbumApi() {
        List<Album> albums = searchService.searchAlbumApi("Yellow", LocaleString.valueOf("ko"));

        assertThat(albums.size())
                .isGreaterThan(0);
    }

    @Test
    void searchSongApi() {
    }
}