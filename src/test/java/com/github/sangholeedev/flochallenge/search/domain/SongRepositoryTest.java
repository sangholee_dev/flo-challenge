package com.github.sangholeedev.flochallenge.search.domain;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class SongRepositoryTest {
    @Autowired
    private SongRepository songRepository;

    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Test
    @DisplayName("노래 데이터가 잘 들어갔는지 테스트")
    public void count() {
        assertThat(songRepository.count()).isGreaterThan(0);
    }

    @Test
    @DisplayName("관계된 앨범조회")
    public void searchAlbum() {
        assertThat(songRepository.findById(10L).get().getAlbum().getLocales()).isNotEmpty();
    }

    private static Stream<Arguments> provideSearchSongApi() {
        return Stream.of(
                Arguments.of("Yellow", "ja"),
                Arguments.of("이문", "ko"),
                Arguments.of("The", "en")
        );
    }

    @ParameterizedTest
    @MethodSource("provideSearchSongApi")
    @DisplayName("노래가 제목과 앨범 지역값으 필터링 되는 테스트")
    public void searchSongApi(String title, String locale) {

        QSong qSong = QSong.song;
        QAlbum qAlbum = QAlbum.album;

        List<Song> songs = jpaQueryFactory.selectFrom(qSong)
                .innerJoin(qSong.album, qAlbum)
                .where(qSong.title.contains(title)
                        .and(qAlbum.locales.any().locale.in("all", locale)))
                .fetchJoin()
                .fetch();

        assertThat(songs).extracting(Song::getTitle)
                .matches(s -> s.contains(title));

    }
}