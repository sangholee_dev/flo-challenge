package com.github.sangholeedev.flochallenge.search.ui;

import com.github.sangholeedev.flochallenge.AbstractControllerTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SearchRestControllerTest extends AbstractControllerTest {

    @Autowired
    private SearchRestController searchRestController;

    @Override
    protected Object controller() {
        return searchRestController;
    }


    private static Stream<Arguments> provideTestSearch() {
        return Stream.of(
                Arguments.of("Yellow", "ja"),
                Arguments.of("이문", "ko"),
                Arguments.of("The", "en")
        );
    }

    @DisplayName("앨범/곡을 제목으로 검색하기")
    @ParameterizedTest
    @MethodSource("provideTestSearch")
    public void search(String title, String locale) throws Exception {
        mockMvc.perform(get("/search")
                .param("title", title)
                .param("locale", locale)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("albums").exists())
                .andExpect(jsonPath("songs").exists())
        ;
    }

    @ParameterizedTest
    @ValueSource(strings = {"ko", "en"})
    @DisplayName("앨범 페이지 검색")
    public void albums(String locale) throws Exception {
        mockMvc.perform(get("/albums")
                .param("locale", locale)
                .param("page", "1")
                .param("size", "10")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("statusCode").exists())
                .andExpect(jsonPath("pages").exists())
                .andExpect(jsonPath("albums").exists())
        ;
    }

    @ParameterizedTest
    @ValueSource(strings = {"ko", "en"})
    @DisplayName("앨범 페이지 검색")
    public void albumstest(String locale) throws Exception {
        mockMvc.perform(get("/albums/test")
                .param("page", "1")
                .param("size", "10")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("statusCode").exists())
                .andExpect(jsonPath("pages").exists())
                .andExpect(jsonPath("albums").exists())
        ;
    }
}