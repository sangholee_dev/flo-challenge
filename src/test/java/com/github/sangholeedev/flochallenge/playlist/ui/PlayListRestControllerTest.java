package com.github.sangholeedev.flochallenge.playlist.ui;

import com.github.sangholeedev.flochallenge.AbstractControllerTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;

import java.nio.charset.StandardCharsets;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlayListRestControllerTest extends AbstractControllerTest {
    @Autowired
    private PlayListRestController playListRestController;

    @Override
    protected Object controller() {
        return playListRestController;
    }

    @Test
    @DisplayName("플레이리스트 생성 테스트")
    public void createPlayList() throws Exception {
        // Given
        PlayListDto playListDto = PlayListDto.builder()
                .name("1 playlist")
                .build();

        // When & Then
        mockMvc.perform(post("/api/playlist/4")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .content(objectMapper.writeValueAsString(playListDto))
                .accept(MediaTypes.HAL_JSON_VALUE)
        )
                .andExpect(status().isCreated())
                .andDo(document("query-playList"))
        ;
    }

    @Test
    @DisplayName("플레이리스트 생성 실패 테스트")
    public void createPlayList_400() throws Exception {
        // When & Then
        mockMvc.perform(post("/api/playlist/4")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name())
                .accept(MediaTypes.HAL_JSON_VALUE)
        )
                .andExpect(status().isBadRequest())
        ;
    }

}