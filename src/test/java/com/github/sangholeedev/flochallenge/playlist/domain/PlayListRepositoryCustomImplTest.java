package com.github.sangholeedev.flochallenge.playlist.domain;

import com.github.sangholeedev.flochallenge.search.domain.QSong;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PlayListRepositoryCustomImplTest {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L})
    public void findByIdFetchJoinSong(Long id) {
        QPlayList qPlayList = QPlayList.playList;
        QSong qSong = QSong.song;

        List<Tuple> playList = jpaQueryFactory
                .select(qPlayList.id, qPlayList.name, qPlayList.userId,
                        qSong.id, qSong.title, qSong.track, qSong.length
                )
                .from(qPlayList)
                .join(qSong)
                .on(qPlayList.songs.any().id.eq(qSong.id))
                .where(qPlayList.id.eq(id))
                .fetchJoin()
                .fetch();

        Tuple first = playList.get(0);
        PlayList findPlayList = new PlayList(
                first.get(qPlayList.id)
                , first.get(qPlayList.name)
                , first.get(qPlayList.userId)
                , playList.stream().map(tuple -> new PlayedSong(
                tuple.get(qSong.id)
                , tuple.get(qSong.title)
                , tuple.get(qSong.track)
                , tuple.get(qSong.length)
        )).collect(Collectors.toList())
        );
        assertThat(findPlayList.getSongs()).extracting(PlayedSong::getTitle).isEmpty();
    }
}