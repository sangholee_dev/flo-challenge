# REST API docs

FLO 챌린지

---

REST API는 완전한 RESTful api 를 구성해보려고 Spring Hatoes 기반으로 구성하였습니다.

HTTP 동사는 가능한 HTTP 및 REST 규약을 지키려고 하였습니다.

하이퍼미디어는 HAL 형식을 따릅니다.

## 인덱스 페이지

Method : `GET`

URL : `/` , `/api`

---

## 플레이리스트 EndPoint

[3.1] PlayList 생성 API

Method :`POST`

URL : `/api/playlist/:userId`

/tabl

[요청 데이터](https://www.notion.so/49ed1c4567a44d35a69f1ef029602733)

---

성공 응답

Status : `201 CREATED`

해당 사용자에 플레이리스트를 추가하였습니다.

성공 응답 예제

    {
        "id": 18,
        "name": "test",
        "userId": 10,
        "songs": [],
        "_links": {
            "self": {
                "href": "http://localhost:5000/api/playlist/10/18"
            },
            "query-playList": {
                "href": "http://localhost:5000"
            },
            "add-playList": {
                "href": "http://localhost:5000/api/playlist/18"
            },
            "delete-playList": {
                "href": "http://localhost:5000/api/playlist/18"
            }
        }
    }

---

실패응답

Status : `400 BAD REQUEST`

필수 파라미터를 넣지 않았습니다.

---

[3.2] Playlist 노래, 앨범 추가 API

Method : `GET`

URL : `/api/playlist/:userId/:id/add/:alubumId`

    `/api/playlist/:userId/:id/add/:alubumId/:songId`

[요청 데이터](https://www.notion.so/03b9b794bcf04f4cbc0f2fe5767039a6)

---

성공 응답

Status : `200 OK`

해당 사용자에 플레이리스트에 곡을 추가 하였습니다.

성공 응답 예제

    {
        "id": 18,
        "name": "test",
        "userId": 10,
        "songs": [
            {
                "id": 4,
                "title": "Please Please Me (1963.3) song-4",
                "track": 4,
                "length": 299
            }
        ],
        "_links": {
            "self": {
                "href": "http://localhost:5000/api/playlist/10/18"
            }
        }
    }

---

실패응답

Status : `404 NOT FOUND`

해당 사용자의 플레이 리스트 또는 앨범 정보 또는 곡 정보를 찾지 못하였습니다.

---

[3.3] Playlist 목록 API

Method : `GET`

URL : `/api/playlist/:userId`

[요청 데이터](https://www.notion.so/c997f43f4bde47848adcf1c0bb3bfc9c)

---

성공 응답

Status : `200 OK`

해당사용자의 플레이리스트 목록을 조회하였습니다.

성공 응답 예제

    {
        "_embedded": {
            "playListList": [
                {
                    "id": 18,
                    "name": "test",
                    "userId": 10,
                    "songs": [
                        {
                            "id": 4,
                            "title": "Please Please Me (1963.3) song-4",
                            "track": 4,
                            "length": 299
                        }
                    ],
                    "_links": {
                        "self": {
                            "href": "http://localhost:5000/api/playlist/10/18"
                        }
                    }
                }
            ]
        },
        "_links": {
            "self": {
                "href": "http://localhost:5000/api/playlist/10"
            }
        }
    }

---

실패응답

Status : `404 NOT FOUND`

해당 사용자의 플레이리스트 정보를 찾지 못하였습니다.

---

[3.4] Playlist 삭제 API

Method : `DELETE`

URL : `/api/playlist/:userId/:id`

[요청 데이터](https://www.notion.so/552ab078ed734f83a36dc42ea0b5b18c)

---

성공 응답

Status : `204 NO CONTENT`

해당 사용자의 플레이리스트를 삭제하였습니다.

---

실패응답

Status : `404 NOT FOUND`

해당 사용자의 플레이리스트 정보를 찾지 못하였습니다.
